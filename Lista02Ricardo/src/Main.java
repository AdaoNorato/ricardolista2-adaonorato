import java.math.BigInteger;

public class Main {

    public static void main(String[] args){
            testMistery(); //18.07
            testMisteryII(); //18.12
            testMisteryIII(); //18.13
            testSum(); //18.8
            testPower(); //18.9
    }

    public static void testMistery(){
       System.out.println("esperado 10*10 , res: "+misteryI( 10,10));
    }

    public static void testMisteryII(){
        int[] array = {1,2,3,4,5,6,7,8,9,10};
        int result = misteryII(array,array.length);
       System.out.printf("esperado a soma de 1 + 2 + 3 + ... + 10 , res : %d%n",result );
    }

    public static void testMisteryIII(){
           int[] array = {1,2,3,4,5,6,7,8,9,10};
           String results = misteryIII(array, 0);
           System.out.println("esperado 1098..321 , res: "+results);
    }



    public static void testSum(){
        System.out.println("esperado 5+4+3+2+1 , res: "+ sum(5,0));
    }

    public static void testPower(){
        System.out.println("esperado 3^4 , res: "+ Power(3,4));
    }





    /**
     * Exercicio 18.7 é um metodo recursivo.
     *
     * @param a valor inteiro, que é somado por ele mesmo até B ser igual a 1.
     * @param b valor inteiro, que é subtraido por 1, se B é diferente de 1 ele chama o próprio metodo até B ser 1.
     * @return o metodo Retorna A * B.
     */
    public static int misteryI(int a, int b){
           return ( b == 1) ? a : a + misteryI(a,b-1);
    }

    /**
     * Exercicio 18.12 é um metodo recursivo.
     * Que, atribui um valor para o elemento de "array2[size-1]" a soma do seu valor com a soma do seu antecessor.
     *
     * @param array2 é um arranjo de numeros inteiros.
     * @param size é a quantidade de elementos que array 2 possui.
     * @return A soma de todos elementos do Array2
     */
    public static int misteryII(int[] array2, int size){
           return ( size == 1) ? array2[0] : array2[size-1] + misteryII(array2,size-1);
    }

    /**
     *  Exercicio  18.13 é um metodo recursivo.
     *  Que, forma uma string com os elementos de array2 em ordem descrescente.
     * @param array2 arranjo de inteiros.
     * @param x X = indice
     * @return Os valores de array2 da direita para a esquerda.
     */
    public static String misteryIII(int[] array2, int x){
           return( x < array2.length )
                   ? String.format("%s%d" , misteryIII(array2, x+1), array2[x])
                   : "";
    }

    /**
     * Exercicio 18.8 apresenta um erro no metodo recursivo onde deve encontrar a soma dos valores de
     * 0 até N:
     *    public static int sum(int n){
     *     return (n == 0)? 0 : n + sum(n);
     *     }
     *
     *     como corrigi-lo:
     *     incluir um novo parametro "x" como indice para a soma.
     *
     * @param n valor inteiro, somar todos os valores de x até esse parametro.
     * @param x indice.
     * @return soma de todos elementos de x para n
     */
    public static int sum(int n, int x){
    return (x == n) ? 0 : n + sum(n-1, x);
    }

    /**
     * Exercicio 18.9
     * Faz eleva a base ao expoente.
     * @param base valor inteiro.
     * @param expoente valor inteiro.
     * @return o valor da base à potencia.
     */
    public static int Power(int base, int expoente){
        int b = base;
        for(int i = 1; i < expoente; i++){
            base *= b;
        }
        return base;
    }


}
